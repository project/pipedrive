<?php

namespace Drupal\pipedrive\Plugin\WebformHandler;

use Drupal\Core\Render\Element;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Pipedrive Webform Handler.
 *
 * @WebformHandler(
 *   id = "pipedrive",
 *   label = @Translation("Pipedrive"),
 *   category = @Translation("External"),
 *   description = @Translation("Sends a webform submission to Pipedrive."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class PipedriveWebformHandler extends WebformHandlerBase {

  /**
   * The pipedrive fields service.
   *
   * @var \Drupal\pipedrive\Service\PipedriveFieldsServiceInterface
   */
  protected $pipedriveFields;

  /**
   * The pipedrive service.
   *
   * @var \Drupal\pipedrive\Service\PipedriveSyncServiceInterface
   */
  protected $pipedriveSync;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->pipedriveFields = $container->get('pipedrive.fields');
    $instance->pipedriveSync = $container->get('pipedrive.sync');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'pipedrive_mapping' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $webform = $this->getWebform();
    // Assemble webform mapping source fields.
    $map_sources = [];
    $elements = $this->webform->getElementsDecoded();
    foreach (Element::children($elements) as $key) {
      if (empty($elements[$key]['#title'])) {
        continue;
      }
      $map_sources[$key] = $elements[$key]['#title'];
    }
    $submission_storage = \Drupal::entityTypeManager()->getStorage('webform_submission');
    $field_definitions = $submission_storage->getFieldDefinitions();
    $field_definitions = $submission_storage->checkFieldDefinitionAccess($webform, $field_definitions);
    foreach ($field_definitions as $key => $field_definition) {
      $map_sources[$key] = $field_definition['title'] . ' (type : ' . $field_definition['type'] . ')';
    }

    $form['pipedrive_mapping'] = [
      '#type' => 'webform_mapping',
      '#title' => $this->t('Webform to Pipedrive mapping'),
      '#description' => $this->t('Only Maps with specified "Pipedrive" will be submitted to Marketo.'),
      '#source__title' => t('Webform Submitted Data'),
      '#destination__title' => t('Pipedrive Field'),
      '#source' => $map_sources,
      '#destination__type' => 'webform_select_other',
      '#destination' => $this->pipedriveFields->getPipedriveFields(),
      '#default_value' => $this->configuration['pipedrive_mapping'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    if ($webform_submission->isCompleted() && $update == FALSE) {
      $data = $webform_submission->getData();
      $this->pipedriveSync->syncObject($data, $this->configuration['pipedrive_mapping']);
    }
  }

}
