<?php

namespace Drupal\pipedrive\Form;

use Pipedrive\APIException;
use Pipedrive\Client;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure pipedrive settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pipedrive_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pipedrive.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API token'),
      '#description' => $this->t('API token for Pipedrive from <a href="@settings">your settings page</a>.', ['@settings' => 'https://app.pipedrive.com/settings/api']),
      '#default_value' => $this->config('pipedrive.settings')->get('api_token'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $apiToken = $form_state->getValue('api_token');
    // First 3 parameters are for OAuth2.
    $client = new Client(NULL, NULL, NULL, $apiToken);
    try {
      $response = $client->getUsers()->getCurrentUserData();
      if ($response->success) {
        $this->messenger()->addStatus($this->t('The API key is valid for the account %account', [
          '%account' => $response->data->companyName,
        ]));
      }
      else {
        $form_state->setErrorByName('api_token', $this->t('The API token does not appear to be valid.'));

      }
    }
    catch (APIException $e) {
      $this->logger('pipedrive')->error('Error testing API key %error', [
        '%error' => $e->getMessage(),
      ]);
      $form_state->setErrorByName('api_token', $this->t('There was an error checking the API key, it may not be valid.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('pipedrive.settings')
      ->set('api_token', $form_state->getValue('api_token'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
