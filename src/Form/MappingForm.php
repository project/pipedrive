<?php

namespace Drupal\pipedrive\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for defining Drupal-pipedrive mappings for entity.
 */
class MappingForm extends FormBase {

  /**
   * The entity type for this form.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Return the entity type this form works on.
   *
   * @return string
   *   The entity type id.
   */
  protected function getEntityType() {
    return $this->entityType;
  }

  /**
   * Get the form id.
   *
   * @inheritDoc
   */
  public function getFormId() {
    return 'pipedrive_' . $this->getEntityType() . '_mappings';
  }

  /**
   * Build the form.
   *
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type = NULL, $bundle = NULL) {

    $this->entityType = $entity_type;

    /**
     * @var \Drupal\pipedrive\Service\PipedriveEntitySettingsServiceInterface $settings
     */
    $settings = \Drupal::service('pipedrive.entity_settings')->getSettings($entity_type, $bundle);

    /**
     * @var \Drupal\Core\Entity\EntityTypeInterface $entity_definition
     */
    $entity_definition = \Drupal::service('entity_type.manager')->getDefinition($entity_type);

    $form['entity_type'] = [
      '#type' => 'hidden',
      '#value' => $entity_type,
    ];

    $form['bundle'] = [
      '#type' => 'hidden',
      '#value' => $bundle,
    ];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('Send new @name_plural to pipedrive',
        ['@name_plural' => $entity_definition->getPluralLabel()]),
      '#title' => $this->t('Enable'),
      '#default_value' => $settings->get('enabled'),
    ];

    $form['pipedrive_mappings'] = \Drupal::service('pipedrive.entitymappingform')->getFieldMappingFormItem($entity_type, $bundle, $settings->get('field_mappings'));

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Change Settings'),
    ];

    return $form;

  }

  /**
   * Form submit handler.
   *
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity_type = $form_state->getValue('entity_type');
    $bundle = $form_state->getValue('bundle');
    $enabled = $form_state->getValue('enabled');
    $mappings = \Drupal::service('pipedrive.entitymappingform')->getFieldMappingFormValues($entity_type, $bundle, $form_state);
    \Drupal::service('pipedrive.entity_settings')->setSettings(
        $entity_type,
        $bundle,
        $enabled,
        $mappings
      );
  }

}
