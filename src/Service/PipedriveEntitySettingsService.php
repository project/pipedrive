<?php

namespace Drupal\pipedrive\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Service for storing and retrieving settings for Pipedrive entity mappings.
 */
class PipedriveEntitySettingsService implements PipedriveEntitySettingsServiceInterface {

  /**
   * A configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * PipedriveEntitySettingsService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Get the key for the settings.
   *
   * @inheritDoc
   */
  public function getSettingsKey($entity_type, $bundle = NULL) {
    if ($bundle == NULL) {
      $key = 'pipedrive.mappings_' . $entity_type;
    }
    else {
      $key = 'pipedrive.mappings_' . $entity_type . '_' . $bundle;
    }
    return $key;
  }

  /**
   * Get existing settings or default.
   *
   * @inheritDoc
   */
  public function getSettings($entity_type, $bundle = NULL) {
    $key = $this->getSettingsKey($entity_type, $bundle);
    $settings = $this->configFactory->get($key);
    // If config exists return the settings.
    if ($settings) {
      return $settings;
    }
    // If no config create defautls.
    $settings = $this->configFactory->getEditable($key);
    $settings->set('enabled', FALSE);
    $settings->set('field_mappings', []);
    return $settings;
  }

  /**
   * Set the settings for the given items.
   *
   * @inheritDoc
   */
  public function setSettings($entity_type, $bundle = NULL, $enabled = FALSE, $mappings = []) {
    $key = $this->getSettingsKey($entity_type, $bundle);
    $config = $this->configFactory->getEditable($key);
    $config->set('enabled', $enabled);
    $config->set('field_mappings', $mappings);
    $config->save();
  }

}
