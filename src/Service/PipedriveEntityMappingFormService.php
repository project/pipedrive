<?php

namespace Drupal\pipedrive\Service;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * A service for createing and processing an entity mappings form.
 */
class PipedriveEntityMappingFormService implements PipedriveEntityMappingFormServiceInterface {

  use StringTranslationTrait;

  /**
   * The field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The pipedrive service.
   *
   * @var \Drupal\pipedrive\Service\PipedriveFieldsServiceInterface
   */
  protected $pipedriveFieldsService;

  /**
   * EntityMappingFormService constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\pipedrive\Service\PipedriveFieldsServiceInterface $pipedrive_field_service
   *   Pipedrive service.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, PipedriveFieldsServiceInterface $pipedrive_field_service) {
    $this->entityFieldManager = $entity_field_manager;
    $this->pipedriveFieldsService = $pipedrive_field_service;
  }

  /**
   * Get the fields that are mappable for this entity.
   *
   * @param string $entity_type
   *   The entity type id.
   * @param string $bundle
   *   The bundle id.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of entity fields that can be mapped.
   */
  protected function getMappableFieldsForEntity($entity_type, $bundle = NULL) {

    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    $fields = array_filter($fields, function ($field) {
        return in_array($field->getType(),
          ['email', 'boolean', 'text', 'number', 'string']);
    });
    return $fields;
  }

  /**
   * Get a form definition for mapping.
   *
   * For the given entity type and bundle generate a form item which
   * will allow the mappings to be edited.
   *
   * @param string $entity_type
   *   The entity type id.
   * @param string $bundle
   *   The bundle ID.
   * @param array $defaults
   *   Default values for the form items.
   *
   * @return array
   *   The form item that can handle the mapping.
   */
  public function getFieldMappingFormItem($entity_type, $bundle = NULL, array $defaults = []) {
    $fields = $this->getMappableFieldsForEntity($entity_type, $bundle);
    $pipedrive_fields = $this->pipedriveFieldsService->getPipedriveFields();

    $map_sources = [
      '#type' => 'fieldset',
      '#title' => $this
        ->t('Mappings'),
    ];
    foreach ($fields as $key => $field) {
      $map_sources[$key] = [
        '#type' => 'select',
        '#title' => $field->getName(),
        '#options' => array_merge(['' => '-- select'], $pipedrive_fields),
        '#default_value' => $defaults[$key],
      ];
    }
    return $map_sources;
  }

  /**
   * Get Mapping Values.
   *
   * @inheritDoc
   */
  public function getFieldMappingFormValues($entity_type, $bundle, $form_state) {
    $fields = $this->getMappableFieldsForEntity($entity_type, $bundle);
    $values = [];
    foreach ($fields as $key => $field) {
      $values[$key] = $form_state->getValue($key);
    }
    return $values;
  }

}
