<?php

namespace Drupal\pipedrive\Service;

/**
 * Interface for getting pipedrive fields.
 */
interface PipedriveFieldsServiceInterface {

  /**
   * Get the fields that can be used for pipedrive.
   *
   * @return array
   *   An array of fields for the used entities.
   */
  public function getPipedriveFields();

}
