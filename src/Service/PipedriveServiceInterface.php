<?php

namespace Drupal\pipedrive\Service;

/**
 * Interface for PipedriveService.
 */
interface PipedriveServiceInterface {

  /**
   * Get an active Pipedrive client.
   *
   * @return \Pipedrive\Client
   *   The initialised pipedrive client.
   */
  public function getClient();

}
