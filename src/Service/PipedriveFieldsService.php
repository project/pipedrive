<?php

namespace Drupal\pipedrive\Service;

/**
 * Service to retrieve and orgainse mappable pipedrive fields.
 */
class PipedriveFieldsService implements PipedriveFieldsServiceInterface {

  /**
   * The pipedrive service.
   *
   * @var \Drupal\pipedrive\Service\PipedriveServiceInterface
   */
  protected $pipedriveService;

  /**
   * Class constructor.
   *
   * @param \Drupal\pipedrive\Service\PipedriveServiceInterface $pipedriveService
   *   The pipedrive service to use.
   */
  public function __construct(PipedriveServiceInterface $pipedriveService) {
    $this->pipedriveService = $pipedriveService;
  }

  /**
   * Get the fields that can be used for pipedrive.
   *
   * @return array
   *   An array of fields for the used entities.
   *
   * @throws \Pipedrive\APIException
   */
  public function getPipedriveFields() {
    $pipedrive_client = $this->pipedriveService->getClient();
    $person_fields_controller = $pipedrive_client->getPersonFields();
    $data = $person_fields_controller->getAllPersonFields();
    $person_fields = $this->buildFieldList($data->data, 'person');

    $company_field_controller = $pipedrive_client->getOrganizationFields();
    $data = $company_field_controller->getAllOrganizationFields([]);
    $organization_fields = $this->buildFieldList($data->data, 'organization');

    $deal_field_controller = $pipedrive_client->getDealFields();
    $data = $deal_field_controller->getAllDealFields([]);
    $deal_fields = $this->buildFieldList($data->data, 'deal');

    return array_merge($person_fields, $organization_fields, $deal_fields);
  }

  /**
   * Build a list of field mappings based on a type.
   *
   * @param array $fields
   *   The fields from the api.
   * @param string $type
   *   The type of the object. To add to the mappings.
   *
   * @return array
   *   An array suitable for using in a webform_mapping destination value.
   */
  protected function buildFieldList(array $fields, string $type) {
    $mapped = [];
    foreach ($fields as $field) {
      if ($field->fieldType == "varchar") {
        $label = $type . ' - ' . $field->name;
        // @todo check for required fields again.
        $mapped[$type . ':' . $field->key] = $label;
      }
    }
    return $mapped;
  }

}
