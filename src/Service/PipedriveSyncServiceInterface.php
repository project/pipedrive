<?php

namespace Drupal\pipedrive\Service;

/**
 * Interface for service syncing items with pipedrive.
 */
interface PipedriveSyncServiceInterface {

  /**
   * Sync the object to pipedrive.
   *
   * @param array $object_values
   *   An array of values keyed by the drupal field name.
   * @param array $mappings
   *   An array of mappings from drupal field names to pipedrive object/fields.
   */
  public function syncObject(array $object_values, array $mappings);

}
