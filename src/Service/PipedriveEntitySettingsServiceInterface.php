<?php

namespace Drupal\pipedrive\Service;

/**
 * Interface for setting pipedrive entity settings.
 */
interface PipedriveEntitySettingsServiceInterface {

  /**
   * Get the key for the settings for this entity type and bundle.
   *
   * @param string $entity_type
   *   The entity type id.
   * @param string $bundle
   *   The bundle id.
   */
  public function getSettingsKey($entity_type, $bundle = NULL);

  /**
   * Get the settings from storage.
   *
   * @param string $entity_type
   *   The entity type id.
   * @param string $bundle
   *   The bundle id.
   */
  public function getSettings($entity_type, $bundle = NULL);

  /**
   * Save the settings for the given entity type and bundle.
   *
   * @param string $entity_type
   *   The entity type id.
   * @param string $bundle
   *   The bundle id.
   * @param bool $enabled
   *   True if the entity is to be passed to pipedrive.
   * @param array $mappings
   *   An array of mappings between entity fields and pipedrive fields.
   */
  public function setSettings(
      $entity_type,
      $bundle = NULL,
      $enabled = FALSE,
      array $mappings = []
    );

}
