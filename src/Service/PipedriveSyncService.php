<?php

namespace Drupal\pipedrive\Service;

use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * Service to sync items to pipedrive.
 */
class PipedriveSyncService implements PipedriveSyncServiceInterface {


  /**
   * Pipedrive client.
   *
   * @var \Pipedrive\Client
   */
  protected $pipedrive;

  /**
   * Logger for the service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructor.
   */
  public function __construct(PipedriveServiceInterface $pipedrive_service, LoggerChannelInterface $loggerChannel) {
    $this->pipedrive = $pipedrive_service->getClient();
    $this->logger = $loggerChannel;
  }

  /**
   * Sync the object with pipedrive.
   *
   * @inheritDoc
   */
  public function syncObject($object_values, $mappings) {
    $user_id = NULL;
    $organization_id = NULL;
    $pipedrive_client = $this->pipedrive;
    $mapped_fields = [];

    foreach ($mappings as $webform_field => $pipedrive_field) {
      [$object, $field] = explode(':', $pipedrive_field);
      $mapped_fields[$object][$field] = $object_values[$webform_field];
    }

    if (array_key_exists('organization', $mapped_fields)) {
      $organization_controller = $pipedrive_client->getOrganizations();
      try {
        /**
         * @var \Pipedrive\Models\Organization $organization
         */
        $organization = $organization_controller->addAnOrganization($mapped_fields['organization']);
        $this->logger->info('Organisation created @org_name', ['@org_name' => $organization->name]);
      }
      catch (\Exception $exception) {
        $this->logger->error('Error saving organization on Pipedrive: @message',
                ['@message' => $exception->getMessage()]);
      }
      if (isset($organization->data->id)) {
        $organization_id = $organization->data->id;
      }
    }
    if (array_key_exists('person', $mapped_fields)) {
      // If we have an email check to see if the user exists.
      // Only do this on insert I think.
      if ($organization_id) {
        $mapped_fields['person']['org_id'] = $organization_id;
      }

      $person_controller = $pipedrive_client->getPersons();
      try {
        /**
         * @var \Pipedrive\Models\Person; $person
         */
        $person = $person_controller->addAPerson($mapped_fields['person']);
        $this->logger->info('Created Person @person', ['@person' => $person->name]);
      }
      catch (\Exception $exception) {
        $this->logger->error('Error saving user on Pipedrive: @message',
            ['@message' => $exception->getMessage()]);
      }
      if (isset($person->data->id)) {
        $user_id = $person->data->id;
      }
    }
    if (array_key_exists('deal', $mapped_fields)) {
      if (!$user_id && !$organization_id) {
        $this->logger->error('Data is present for opportunity but no user or organization is present');
      }
      else {
        if ($user_id) {
          $mapped_fields['deal']['person_id'] = $user_id;
        }
        if ($organization_id) {
          $mapped_fields['deal']['org_id'] = $organization_id;
        }
        $deal_controller = $pipedrive_client->getDeals();
        try {
          /**
           * @var \Pipedrive\Models\Deal; $deal
           */
          $deal = $deal_controller->addADeal($mapped_fields['deal']);
          $this->logger->info('Created Deal @deal_id', ['@deal_id' => $deal->id]);
        }
        catch (\Exception $exception) {
          $this->logger->error('Error saving deal on Pipedrive: @message',
              ['@message' => $exception->getMessage()]);
        }
      }
    }
    // @todo return array of IDs of items created.
  }

}
