<?php

namespace Drupal\pipedrive\Service;

use Pipedrive\Client;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides a service from which to access the Pipedrive API.
 */
class PipedriveService implements PipedriveServiceInterface {

  /**
   * Config for the service.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructor for service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('pipedrive.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getClient() {
    $api_token = $this->config->get('api_token');
    return new Client(NULL, NULL, NULL, $api_token);
  }

}
