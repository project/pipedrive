# Pipedrive for Drupal

This module by itself will provide a Pipedrive service that wraps the Pipedrive API.
This can be used by other modules to communicate with Pipedrive.

This will load the Pipedrive client with an API key configured in Drupal.

```
$client = \Drupal::service('pipedrive')->getClient();
```

However, it also has a Webform handler which can post Webform submissions to Pipedrive.

Currently, it will offer to create mappings for Person, Organization and Deals.

It will link a person to an organization if both have fields mapped.

It will link any person or organization to a deal if a deal is created.

It doesn't currently report errors, or check that required fields are mapped.

## Previous maintainer

D7 module by Steven Monetti <https://www.drupal.org/user/741388>
